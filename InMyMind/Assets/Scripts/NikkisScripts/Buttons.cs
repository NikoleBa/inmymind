using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject credits;
    /* public GameObject tutorial;
     public GameObject canvas;
     */
    public GameObject titleElements;


    public void PlayGame()
    {
        SceneManager.LoadScene("FirstLook");

    }

    public void Credits()
    {
        credits.SetActive(true);
        titleElements.SetActive(false);
    }

    public void NoCredits()
    {
        credits.SetActive(false);
        titleElements.SetActive(true);
    }

    /*public void Tutorial()
    {
        Debug.Log("Tutorial");
        tutorial.SetActive(true);
        titleElements.SetActive(false);
    }
  

    public void NoTutorial()
    {
        tutorial.SetActive(false);
        titleElements.SetActive(true);
    }
  */

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

}
