﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitalPenguin.Unity.Gating
{
    [AddComponentMenu("Gated")]
    public class Gated : MonoBehaviour
    {

        public UnityEvent onProceed;
        public UnityEvent onBlocked;

        [SerializeField]
        private List<GateCondition> conditions = new List<GateCondition>();

        void Start()
        {
            bool tummmyGameComplete = PlayerPrefs.GetInt("Tummy", 0)>0;
            ConditionFulfilled("Tummy", tummmyGameComplete);

            bool loveHandleGameComplete = PlayerPrefs.GetInt("Lovehandles", 0) > 0;
            ConditionFulfilled("Lovehandles", loveHandleGameComplete);

            bool strechmarksGameComplete = PlayerPrefs.GetInt("Stretchmarks", 0) > 0;
            ConditionFulfilled("Stretchmarks", strechmarksGameComplete);

            CheckConditions();
        }

        public void AddCondition(string name)
        {
            conditions.Add(new GateCondition(name));
        }

        public void ConditionFulfilled(GameObject gameObject)
        {
            ConditionFulfilled(gameObject.name, true);
        }

        public void ConditionFailed(GameObject gameObject)
        {
            ConditionFulfilled(gameObject.name, false);
        }

        public void ConditionFulfilled(string name)
        {
            ConditionFulfilled(name, true);
        }

        public void ConditionFailed(string name)
        {
            ConditionFulfilled(name, false);
        }

        public void ConditionFulfilled(string name, bool fulfuilled)
        {
            GateCondition gateCondition = conditions.Find(c => c.Name == name);
            if (gateCondition != null)
            {
                gateCondition.fulfilled = fulfuilled;
                CheckConditions();
            }
        }

        private void CheckConditions()
        {
            foreach (GateCondition gateCondition in conditions)
            {
                if (gateCondition.name == null || gateCondition.name.Length == 0) 
                    continue;
                if (!gateCondition.fulfilled) {
                    onBlocked?.Invoke();
                    return;
                }
            }

            AllConditionsMet();
        }

        private void AllConditionsMet()
        {
            onProceed?.Invoke();
            this.enabled = false;
        }
    }

    [Serializable]
    public class GateCondition
    {
        public string name;
        public bool fulfilled = false;

        public GateCondition()
        {
        }

        public GateCondition(string name)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }
    }
}