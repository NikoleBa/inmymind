using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MonologSystem : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private GameObject continueButton;
    [SerializeField] private string[] sentences;
    private int index;

    [SerializeField] private float typingSpeed;

    [SerializeField] private GameObject goBackButton;
    [SerializeField] private GameObject tummyButton;
    [SerializeField] private GameObject stretchButton;
    [SerializeField] private GameObject flawButton;
    [SerializeField] private GameObject uncomfortable;
    [SerializeField] private GameObject abstractBody;
    [SerializeField] private GameObject textField;


    private void Start()
    {
        StartCoroutine(Type());
      uncomfortable.SetActive(false);
      abstractBody.SetActive(true);
      textField.SetActive(false);

    }

    private void Update()
    {
        if (text.text == sentences[index])
        {
            continueButton.SetActive(true);
            goBackButton.SetActive(false);
            flawButton.SetActive(false);
            tummyButton.SetActive(false);
            stretchButton.SetActive(false);

        }
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            text.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
               continueButton.SetActive(false);
               
        if (index < sentences.Length - 1)
        { 
            index++;
            text.text = "";
            StartCoroutine(Type());
        }
        else
        {
            text.text = "";
            
            goBackButton.SetActive(true);
            flawButton.SetActive(true);
            tummyButton.SetActive(true);
            stretchButton.SetActive(true);
            uncomfortable.SetActive(true);
            abstractBody.SetActive(false);
            textField.SetActive(true);
        }
    }
}
