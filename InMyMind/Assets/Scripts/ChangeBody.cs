using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class ChangeBody : MonoBehaviour
{

    [SerializeField] private GameObject[] bodies;
    
    public int selectedBeauty = 0;

    public void nextBody()
    {
        bodies[selectedBeauty].SetActive(false);
        
        selectedBeauty = (selectedBeauty + 1) % bodies.Length;
        bodies[selectedBeauty].SetActive(true);
    }

    public void previousBody()
    {
        bodies[selectedBeauty].SetActive(false);
        selectedBeauty--;
        if (selectedBeauty < 0)
        {
            selectedBeauty += bodies.Length;
        }
        bodies[selectedBeauty].SetActive(true);
    }
}

