﻿using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
    {
        public static bool GameIsPaused = false;
        public GameObject pauseMenueUI;
    
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (GameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }

        public void StopGame()
        {
            Application.Quit();
        }

        public void BackToMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    

        void Pause()
        {
            pauseMenueUI.SetActive(true);
            GameIsPaused = true;
        }

        void Resume()
        {
            pauseMenueUI.SetActive(false);
            GameIsPaused = false;
        }
}
