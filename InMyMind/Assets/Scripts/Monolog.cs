﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Monolog : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private GameObject continueButton;
    [SerializeField] private string[] sentences;
    private int index;

    [SerializeField] private float typingSpeed;

    [SerializeField] private GameObject goBackButton;
    [SerializeField] private GameObject heart;
    


    private void Start()
    {
        StartCoroutine(Type());
        

    }

    private void Update()
    {
        if (text.text == sentences[index])
        {
            continueButton.SetActive(true);
            heart.SetActive(false);
            goBackButton.SetActive(false);

        }
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            text.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
               continueButton.SetActive(false);
               
        if (index < sentences.Length - 1)
        { 
            index++;
            text.text = "";
            StartCoroutine(Type());
        }
        else
        {
            text.text = "";
            
            heart.SetActive(true);
            continueButton.SetActive(false);
            goBackButton.SetActive(true);
        }
    }
}
