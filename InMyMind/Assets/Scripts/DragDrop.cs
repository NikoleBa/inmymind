using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngineInternal;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private bool isBeingHeld = false;
    [SerializeField] private AudioSource bell;
    [SerializeField] private GameObject flaw;

    public string gameName;

    private void Start()
    {
        PlayerPrefs.SetInt(gameName, 0);
    }

    private void Update()
    {
        
        flaw.SetActive(true);
        
        if (isBeingHeld == true)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY,  0);
        }
    }
    
    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.localPosition.x;
            startPosY = mousePos.y - this.transform.localPosition.y;
            
            isBeingHeld = true;
        }
    }

    private void OnMouseUp()
    {
        isBeingHeld = false;
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag == "Flaw")
        {
            Destroy(collider.gameObject);
            bell.Play();
            // to check if a game is done, do enable/disbale the end-button
            PlayerPrefs.SetInt(gameName, 1);
        }
        
    }
}
